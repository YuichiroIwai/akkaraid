package akkaraid

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.sharding.ClusterSharding
import akkaraid.AppHandler.{AllocatePlayers, Start}
import akkaraid.core.{Roles, StatsManager}
import akkaraid.models.World.{AllocatePlayer, PutBoss}
import akkaraid.models.{Player, World}
import akkaraid.shared.{AppStats, Constants, Pos}
import apparaid.CloudWatchConnector
import com.amazonaws.regions.Regions
import com.amazonaws.services.simplesystemsmanagement.{AWSSimpleSystemsManagementClient, AWSSimpleSystemsManagementClientBuilder}
import com.typesafe.config.ConfigFactory
import com.yuiwai.akka.cluster.ecs.Bootstrap

import scala.util.Random

object Boot extends App {
  val env = args.head
  val bootConfig = BootSetting(env)
  val config = ConfigFactory.load(env)
  val systemName = "akkaraid-system"
  implicit val system: ActorSystem = if (bootConfig.isEcs) {
    val ssm = AWSSimpleSystemsManagementClientBuilder
      .standard()
      .withRegion(Regions.US_WEST_2)
      .build()
      .asInstanceOf[AWSSimpleSystemsManagementClient]
    val bootstrap = Bootstrap.withSSM(ssm)
    if (bootConfig.isHandler) {
      bootstrap.clearNodes(systemName)
    }
    bootstrap.start(systemName, Some(config))
  } else ActorSystem(systemName, Some(config))
  config.getString("akka.cluster.sharding.role") match {
    case Roles.World => World.startSharding
    case Roles.Player => Player.startSharding
    case Roles.Handler => AppHandler.run(bootConfig.isEcs)
  }
  case class BootSetting(argv: String) {
    lazy val isEcs: Boolean = argv.endsWith("ecs")
    lazy val isHandler: Boolean = argv.startsWith("handler")
  }
}

class AppHandler(worldRegion: ActorRef, useCw: Boolean) extends Actor {
  implicit val node = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  val statsPublisher: Seq[ActorRef] = if (useCw) {
    Seq(context.actorOf(Props[CloudWatchPublisher]))
  } else Seq.empty
  val stats: ActorRef = context.actorOf(StatsManager.props(statsPublisher), "stats-manager")
  Cluster(context.system) registerOnMemberUp {
    self ! Start
  }
  def initialize(): Unit = {
    stats ! StatsManager.Start
    import context.dispatcher

    import scala.concurrent.duration._
    Pos.bossFields.foreach(worldRegion ! PutBoss(_))
    context.system.scheduler.schedule(0.second, Constants.playerAllocateInterval.second, self, AllocatePlayers)
  }
  override def receive: Receive = {
    case Start => initialize()
    case AllocatePlayers => (1 to Constants.playerAllocateOnce).foreach(_ => worldRegion ! AllocatePlayer(Random.nextInt()))
    case _ =>
  }
}
object AppHandler {
  def props(worldRegion: ActorRef, useCw: Boolean): Props = Props(classOf[AppHandler], worldRegion, useCw)
  def run(useCw: Boolean)(implicit system: ActorSystem): Unit = {
    val worldRegion = ClusterSharding(system).startProxy(Roles.World, Some(Roles.World), World.entityIdExtractor, World.shardIdExtractor)
    system.actorOf(props(worldRegion, useCw), "app-handler")
  }
  case object Start
  case object AllocatePlayers
}

class SimpleTracer extends Actor {
  override def receive: Receive = {
    case any => println(any)
  }
}

class CloudWatchPublisher extends Actor {
  override def receive: Receive = {
    case appStats: AppStats => CloudWatchConnector.request(appStats)
  }
}