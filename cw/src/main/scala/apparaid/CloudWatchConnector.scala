package apparaid

import akkaraid.shared.{AppStats, Pos}
import com.amazonaws.regions.Regions
import com.amazonaws.services.cloudwatch.model.{Dimension, MetricDatum, PutMetricDataRequest, StandardUnit}
import com.amazonaws.services.cloudwatch.{AmazonCloudWatch, AmazonCloudWatchClientBuilder}

object CloudWatchConnector {
  val namespace = "AKKARAID"
  val cw: AmazonCloudWatch = AmazonCloudWatchClientBuilder
    .standard()
    .withRegion(Regions.US_WEST_2
    )
    .build()
  val bossDimension: Dimension = new Dimension()
    .withName("BOSS")
    .withValue("HP")
  val fighterWaiting: Dimension = new Dimension()
    .withName("FIGHTER")
    .withValue("WAITING")
  val fighterGoing: Dimension = new Dimension()
    .withName("FIGHTER")
    .withValue("GOING")
  val fighterFighting: Dimension = new Dimension()
    .withName("FIGHTER")
    .withValue("FIGHTING")
  val fighterReturning: Dimension = new Dimension()
    .withName("FIGHTER")
    .withValue("RETURNING")

  def posToName(posOpt: Option[Pos]): String = posOpt match {
    case Some(pos) => s"${pos.x}-${pos.y}"
    case None => "none"
  }
  def request(name: String, value: Double, dimension: Dimension): Unit = {
    val datum: MetricDatum = new MetricDatum()
      .withMetricName(name)
      .withUnit(StandardUnit.None)
      .withValue(value)
      .withDimensions(dimension)
    request(datum)
  }
  def request(datum: MetricDatum): Unit = {
    val r = new PutMetricDataRequest()
      .withNamespace(namespace)
      .withMetricData(datum)
    cw.putMetricData(r)
  }
  def request(appStats: AppStats): Unit = {
    appStats.bossStats.foreach { case (pos, bossStats) =>
      request(posToName(Some(pos)), bossStats.hp.toDouble, bossDimension)
    }
    import appStats.fighterStats
    request("waiting", fighterStats.waiting, fighterWaiting)
    request("going", fighterStats.going, fighterGoing)
    request("fighting", fighterStats.fighting, fighterFighting)
    request("returning", fighterStats.returning, fighterReturning)
  }
}
