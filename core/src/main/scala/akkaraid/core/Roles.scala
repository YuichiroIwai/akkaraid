package akkaraid.core

object Roles {
  val World = "world"
  val Player = "player"
  val Boss = "boss"
  val Handler = "handler"
}
