package akkaraid.core

import akka.actor.Actor

import scala.concurrent.duration._

trait JobScheduler {
  this: Actor =>
  import context.dispatcher
  def delay(sec: Int, message: MessageLike): Unit = {
    context.system.scheduler.scheduleOnce(sec.second, self, message)
  }
}
