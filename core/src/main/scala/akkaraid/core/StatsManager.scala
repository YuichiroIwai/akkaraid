package akkaraid.core

import akka.actor.{Actor, ActorRef, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator._
import akka.cluster.ddata._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{SubscribeAck, Subscribe => PubSubSubscribe}
import akkaraid.models.Boss.{Damaged, Defeated, Recovered}
import akkaraid.models.Fighter._
import akkaraid.models.World.{BossPut, PlayerAllocated}
import akkaraid.models.{Boss, World}
import akkaraid.shared.AppStats

import scala.concurrent.duration._

class StatsManager(publishActors: Seq[ActorRef]) extends Actor {
  import StatsManager._
  import context.dispatcher
  implicit val node = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  val mediator: ActorRef = DistributedPubSub(context.system).mediator
  var appStats: AppStats = AppStats.empty
  def initialize(): Unit = {
    mediator ! PubSubSubscribe(World.playerAllocated, self)
    mediator ! PubSubSubscribe(World.bossPut, self)
    mediator ! PubSubSubscribe(Boss.damaged, self)
    mediator ! PubSubSubscribe(Boss.defeated, self)
    mediator ! PubSubSubscribe(Boss.recovered, self)
    context.system.scheduler.schedule(0.second, 1.second, self, Publish)
  }
  override def receive: Receive = {
    case Start => initialize()
    case Publish =>
      replicator ! Get(fighterStatsKey, ReadLocal)
      publishActors foreach (_ ! appStats)
    case g@GetSuccess(StatsManager.fighterStatsKey, _) =>
      val stateMap = g.get(fighterStatsKey).entries.map {
        case (state, counter) => state -> counter.value.toInt
      }
      modify(_.modifyFighterStats(_.copy(
        stateMap.getOrElse(Waiting, 0), stateMap.getOrElse(Going, 0), stateMap.getOrElse(Fighting, 0), stateMap.getOrElse(Returning, 0)
      )))
    case PlayerAllocated(_) => modify(_.addPlayer())
    case BossPut(pos, _, hp) => modify(_.putBoss(pos, hp))
    case Damaged(pos, _, hp, level) => modify(_.putBoss(pos, hp, level))
    case Defeated(pos, _, level) => modify(_.putBoss(pos, 0, level))
    case Recovered(pos, hp, level) => modify(_.putBoss(pos, hp, level))
    case SubscribeAck(_) =>
    case _ =>
  }
  def modify(f: AppStats => AppStats): Unit = appStats = f(appStats)
}

object StatsManager {
  val fighterStatsKey: ORMapKey[FighterState, PNCounter] = ORMapKey[FighterState, PNCounter]("fighterStatsKey")
  def props(publishActors: Seq[ActorRef]): Props = Props(classOf[StatsManager], publishActors)

  def onFighterStateChanged(replicator: ActorRef)(beforeState: FighterState, afterState: FighterState)(implicit node: Cluster): Unit = {
    if (beforeState != afterState) {
      replicator ! Update(fighterStatsKey, ORMap.empty[FighterState, PNCounter], WriteLocal) {
        _.updated(node, beforeState, PNCounter.empty)(_.decrement(node))
      }
    }
    replicator ! Update(fighterStatsKey, ORMap.empty[FighterState, PNCounter], WriteLocal) {
      _.updated(node, afterState, PNCounter.empty)(_.increment(node))
    }
  }

  // Command
  sealed trait StatsCommand extends CommandLike
  case object Start extends StatsCommand
  case object Publish extends StatsCommand
}