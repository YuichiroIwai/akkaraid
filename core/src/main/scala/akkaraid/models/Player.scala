package akkaraid.models

import akka.actor.{ActorRef, ActorSystem, FSM, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.Replicator._
import akka.cluster.pubsub.DistributedPubSubMediator.SubscribeAck
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import akkaraid.core.{CommandLike, Roles}
import akkaraid.models.Fighter.GoToAttack
import akkaraid.models.Player.{PlayerDataLike, PlayerState}
import akkaraid.models.World.BossPut
import akkaraid.shared.{Constants, Pos}

import scala.concurrent.duration._

class Player extends FSM[PlayerState, PlayerDataLike] {
  import Player._
  implicit val node = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  val worldRegion: ActorRef = ClusterSharding(context.system).startProxy(Roles.World, Some(Roles.World), World.entityIdExtractor, World.shardIdExtractor)

  override def preStart(): Unit = {
    super.preStart()
    replicator ! Subscribe(Boss.dataKey, self)
  }

  startWith(Uninitialized, EmptyPlayerData)
  when(Uninitialized) {
    case Event(Allocate(worldId, pos), _) =>
      goto(Idle) using PlayerData(worldId, pos, context.actorOf(Fighter.props(pos, self, Constants.fighterInitialAttack, Constants.fighterInitialSpeed), s"fighter-${pos.x}-${pos.y}"), None)
  }

  when(Idle, 1.second) {
    case Event(StateTimeout, pd: PlayerData) =>
      pd.nearestBoss match {
        case None => stay()
        case target@Some(bossPos) =>
          val msg = GoToAttack(bossPos, pd.bosses(bossPos))
          pd.fighter ! msg
          goto(Attacking) using pd.copy(targetPos = target)
      }
  }

  when(Attacking) {
    case Event(Return(_), _) =>
      goto(Idle)
  }

  whenUnhandled {
    case Event(c@Changed(Boss.dataKey), pd: PlayerData) =>
      stay() using pd.copy(bosses = c.get(Boss.dataKey).elements.toMap)
    case Event(Changed(Boss.dataKey), _) => stay()
    case Event(BossPut(bossPos, boss, _), pd: PlayerData) =>
      stay() using pd.putBoss(bossPos, boss)
    case Event(BossPut(_, _, _), _) => stay()
    case Event(Logout, _) => stop()
    case Event(SubscribeAck(_), _) => stay()
  }

  initialize()
}

object Player {
  val fighterDeparted = "fighterDeparted"
  val fighterReturned = "fighterReturned"
  def props: Props = Props[Player]
  def startSharding(implicit system: ActorSystem): Unit = {
    ClusterSharding(system).start(Roles.Player, props, ClusterShardingSettings(system), entityIdExtractor, shardIdExtractor)
  }
  val numOfShards = 32
  val entityIdExtractor: ShardRegion.ExtractEntityId = {
    case msg@Allocate(_, pos) => (pos.hashCode().toString, msg)
  }
  val shardIdExtractor: ShardRegion.ExtractShardId = {
    case Allocate(_, pos) => (pos.hashCode() % numOfShards).toString
  }

  // Command
  sealed trait PlayerCommand extends CommandLike
  case class Allocate(areaId: Int, pos: Pos) extends PlayerCommand
  case class Return(bossPos: Pos) extends PlayerCommand
  case object Logout extends PlayerCommand

  // State
  sealed trait PlayerState
  case object Uninitialized extends PlayerState
  case object Idle extends PlayerState
  case object Attacking extends PlayerState

  // Data
  sealed trait PlayerDataLike
  case object EmptyPlayerData extends PlayerDataLike
  final case class PlayerData(
    worldId: Int,
    pos: Pos,
    fighter: ActorRef,
    targetPos: Option[Pos] = None,
    bosses: Map[Pos, ActorRef] = Map.empty
  ) extends PlayerDataLike {
    def putBoss(pos: Pos, boss: ActorRef): PlayerData = copy(bosses = bosses.updated(pos, boss))
    def nearestBoss: Option[Pos] = bosses.foldLeft[Option[(Pos, Double)]](None) {
      case (acc, (bossPos, _)) =>
        acc match {
          case None =>
            Some(bossPos, pos.distance(bossPos))
          case Some((_, min)) =>
            val d = pos.distance(bossPos)
            if (d < min) Some(bossPos, d)
            else acc
        }
    } map { case (p, _) => p }
  }
}