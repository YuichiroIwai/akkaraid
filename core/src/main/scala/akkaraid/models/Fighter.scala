package akkaraid.models

import akka.actor.{ActorRef, FSM, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akkaraid.core.{CommandLike, EventLike, JobScheduler}
import akkaraid.models.Boss.{AlreadyDied, Attack, Damaged, Defeated}
import akkaraid.models.Fighter.{FighterData, FighterState}
import akkaraid.models.Player.Return
import akkaraid.shared.Pos

class Fighter(playerPos: Pos, player: ActorRef, attack: Int, speed: Int) extends FSM[FighterState, FighterData] with JobScheduler {
  import Fighter._
  implicit val node = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  def targetPos: Option[Pos] = stateData match {
    case NoTarget => None
    case LockedOn((pos, _)) => Some(pos)
  }
  def onChangeState(state: FighterState): Unit = {
    implicit val system = context.system
    import akkaraid.core.StatsManager.onFighterStateChanged
    onFighterStateChanged(replicator)(stateName, state)
  }

  startWith(Waiting, NoTarget)

  when(Waiting) {
    case Event(GoToAttack(bossPos, boss), _) =>
      delay((playerPos.distance(bossPos) / speed).toInt, Arrived)
      goto(Going) using LockedOn((bossPos, boss))
  }

  when(Going) {
    case Event(Arrived, LockedOn((_, boss))) =>
      boss ! Attack(attack)
      goto(Fighting)
  }

  when(Fighting) {
    case Event(Damaged(pos, _, _, _), _) =>
      delay(2, Fought(pos))
      stay()
    case Event(Defeated(pos, _, _), _) =>
      delay(2, Fought(pos))
      stay()
    case Event(Fought(_), LockedOn((bossPos, _))) =>
      delay((playerPos.distance(bossPos) / speed).toInt, Arrived)
      goto(Returning)
    case Event(AlreadyDied(bossPos), LockedOn((_, _))) =>
      delay((playerPos.distance(bossPos) / speed).toInt, Arrived)
      goto(Returning)
  }

  when(Returning) {
    case Event(Arrived, LockedOn((pos, _))) =>
      player ! Return(pos)
      goto(Waiting)
  }

  onTransition {
    case _ -> Waiting => onChangeState(Waiting)
    case _ -> Going => onChangeState(Going)
    case _ -> Fighting => onChangeState(Fighting)
    case _ -> Returning => onChangeState(Returning)
    case _ =>
  }

  initialize()
}

object Fighter {
  val fought = "fought"
  def props(playerPos: Pos, player: ActorRef, attack: Int, speed: Int): Props = Props(classOf[Fighter], playerPos, player, attack, speed)

  // Command
  sealed trait FighterCommand extends CommandLike
  final case class GoToAttack(pos: Pos, boss: ActorRef) extends FighterCommand

  // Event
  sealed trait FighterEvent extends EventLike
  case object Arrived extends FighterEvent
  final case class Fought(pos: Pos) extends FighterEvent

  // State
  sealed trait FighterState
  case object Waiting extends FighterState
  case object Going extends FighterState
  case object Fighting extends FighterState
  case object Returning extends FighterState

  // Data
  sealed trait FighterData
  final case class LockedOn(targetBoss: (Pos, ActorRef)) extends FighterData
  object NoTarget extends FighterData
}