package akkaraid.models

import akka.actor.{ActorRef, FSM, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator.{Update, UpdateSuccess, WriteLocal}
import akka.cluster.ddata.{DistributedData, ORSet, ORSetKey}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akkaraid.core.{CommandLike, EventLike}
import akkaraid.models.Boss.{BossData, BossState}
import akkaraid.shared.{Constants, Pos}

import scala.concurrent.duration._

class Boss(pos: Pos, initialHp: Long) extends FSM[BossState, BossData] {
  import Boss._
  import context.dispatcher
  implicit val node: Cluster = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  val mediator: ActorRef = DistributedPubSub(context.system).mediator

  startWith(Alive, BossStatus(1, initialHp))

  when(Alive) {
    case Event(Attack(attack), BossStatus(currentLevel, currentHp)) =>
      currentHp - attack match {
        case hp if hp <= 0 =>
          val msg = Defeated(pos, 0, currentLevel)
          mediator ! Publish(defeated, msg)
          replicator ! Update(dataKey, ORSet.empty[(Pos, ActorRef)], WriteLocal)(_ - (pos, self))
          sender() ! msg
          context.system.scheduler.scheduleOnce(Constants.bossRecoverInterval.second, self, Recover)
          goto(Dead) using BossStatus(currentLevel, 0)
        case hp =>
          val msg = Damaged(pos, attack, hp, currentLevel)
          mediator ! Publish(damaged, msg)
          sender() ! msg
          stay() using BossStatus(currentLevel, hp)
      }
  }

  when(Dead) {
    case Event(Attack(_), _) =>
      val msg = AlreadyDied(pos)
      sender() ! msg
      stay()
    case Event(Recover, BossStatus(currentLevel, _)) =>
      val level = currentLevel + 1
      val hp = initialHp * level
      val msg = Recovered(pos, hp, level)
      mediator ! Publish(recovered, msg)
      replicator ! Update(dataKey, ORSet.empty[(Pos, ActorRef)], WriteLocal)(_ + (pos, self))
      goto(Alive) using BossStatus(level, hp)
    case _ => stay()
  }

  whenUnhandled {
    case Event(UpdateSuccess(_, _), _) => stay()
  }

  initialize()
}

object Boss {
  val damaged = "damaged"
  val defeated = "defeated"
  val recovered = "recovered"
  val dataKey: ORSetKey[(Pos, ActorRef)] = ORSetKey[(Pos, ActorRef)]("boss")
  def props(pos: Pos, initialHp: Long): Props = Props(classOf[Boss], pos, initialHp)

  // Command
  sealed trait BossCommand extends CommandLike
  final case class Attack(attack: Int) extends BossCommand
  case object Recover extends BossCommand

  // Event
  sealed trait BossEvent extends EventLike
  final case class Damaged(pos: Pos, damage: Int, currentHp: Long, level: Int) extends BossEvent
  final case class Defeated(pos: Pos, damage: Int, level: Int) extends BossEvent
  final case class Recovered(pos: Pos, hp: Long, level: Int) extends BossEvent
  final case class AlreadyDied(pos: Pos) extends BossEvent

  // State
  sealed trait BossState
  case object Alive extends BossState
  case object Dead extends BossState

  // Data
  sealed trait BossData
  final case class BossStatus(level: Int, hp: Long) extends BossData
}