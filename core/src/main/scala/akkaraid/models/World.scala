package akkaraid.models

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator.{Update, WriteLocal}
import akka.cluster.ddata.{DistributedData, ORSet}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import akkaraid.core.{CommandLike, EventLike, Roles}
import akkaraid.models.Player.Allocate
import akkaraid.shared.{Area, Constants, Pos}

class World extends Actor {
  import World._
  implicit val node = Cluster(context.system)
  val replicator: ActorRef = DistributedData(context.system).replicator
  val mediator: ActorRef = DistributedPubSub(context.system).mediator
  val playerRegion: ActorRef = ClusterSharding(context.system).shardRegion(Roles.Player)
  var players: Set[Pos] = Set.empty
  var bosses: Map[Pos, ActorRef] = Map.empty
  override def receive: Receive = {
    case AllocatePlayer(seed) =>
      val areaId: Int = Math.abs(seed) % numOfShards
      mediator ! Publish(World.playerAllocated, PlayerAllocated(allocatePlayer(areaId)))
    case PutBoss(pos) =>
      val bossRef: ActorRef = putBoss(pos)
      replicator ! Update(Boss.dataKey, ORSet.empty[(Pos, ActorRef)], WriteLocal)(_ + (pos, bossRef))
      mediator ! Publish(World.bossPut, BossPut(pos, bossRef, Constants.bossInitialHp))
  }
  def allocatePlayer(areaId: Int): Pos = {
    Pos.randomFromPlayerField(areaId) match {
      case p if players.contains(p) => allocatePlayer(areaId)
      case p =>
        playerRegion ! Allocate(areaId, p)
        players = players + p
        p
    }
  }
  def putBoss(pos: Pos): ActorRef = {
    if (!bosses.contains(pos)) {
      bosses = bosses.updated(pos, context.actorOf(Boss.props(pos, Constants.bossInitialHp), s"boss-${pos.x}-${pos.y}"))
    }
    bosses(pos)
  }
}

object World {
  val bossPut = "bossPut"
  val playerAllocated = "playerAllocated"
  def props: Props = Props[World]
  def startSharding(implicit system: ActorSystem): Unit = {
    DistributedPubSub(system).mediator
    ClusterSharding(system).start(Roles.World, props, ClusterShardingSettings(system), entityIdExtractor, shardIdExtractor)
    ClusterSharding(system).startProxy(Roles.Player, Some(Roles.Player), Player.entityIdExtractor, Player.shardIdExtractor)
  }
  val numOfShards = 4
  val entityIdExtractor: ShardRegion.ExtractEntityId = {
    case msg@AllocatePlayer(seed) => ((Math.abs(seed) % numOfShards).toString, msg)
    case msg@PutBoss(pos) => ((Area.fromPos(pos) % numOfShards).toString, msg)
  }
  val shardIdExtractor: ShardRegion.ExtractShardId = {
    case AllocatePlayer(seed) => (Math.abs(seed) % numOfShards).toString
    case PutBoss(pos) => (Area.fromPos(pos) % numOfShards).toString
  }

  case class Bosses(bosses: Map[Pos, ActorRef])

  // Command
  sealed trait WorldCommand extends CommandLike
  final case class AllocatePlayer(seed: Int) extends WorldCommand
  final case class PutBoss(pos: Pos)

  // Event
  sealed trait WorldEvent extends EventLike
  final case class PlayerAllocated(pos: Pos) extends WorldEvent
  final case class BossPut(pos: Pos, boss: ActorRef, hp: Long) extends WorldEvent
}