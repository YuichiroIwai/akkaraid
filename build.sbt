import com.amazonaws.regions.{Region, Regions}
val commonSettings = Seq(
  version := "1.3.1",
  scalaVersion := "2.12.3"
)
lazy val core = (project in file("core"))
  .settings(
    commonSettings,
    name := "akkaraid-core",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % "2.5.3",
      "com.typesafe.akka" %% "akka-remote" % "2.5.3",
      "com.typesafe.akka" %% "akka-cluster-sharding" % "2.5.3",
      "com.typesafe.akka" %% "akka-cluster-tools" % "2.5.3",
      "com.typesafe.akka" %% "akka-testkit" % "2.5.3",
      "org.scalatest" %% "scalatest" % "3.0.1" % Test,
      "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test
    )
  )
  .dependsOn(shared)

lazy val cw = (project in file("cw"))
  .settings(
    commonSettings,
    name := "akkaraid-cw",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % "2.5.3",
      "com.typesafe.akka" %% "akka-remote" % "2.5.3",
      "com.amazonaws" % "aws-java-sdk-cloudwatch" % "1.11.175"
    )
  )
  .dependsOn(shared)

lazy val shared = (project in file("shared"))
  .settings(
    commonSettings,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % "2.5.3"
    )
  )

lazy val boot = (project in file("boot"))
  .settings(
    commonSettings,
    name := "akkaraid",
    resolvers ++= Seq(
      "yuiwai repo" at "https://s3-us-west-2.amazonaws.com/repo.yuiwai.com"
    ),
    libraryDependencies ++= Seq(
      "com.yuiwai" %% "akka-cluster-ecs" % "0.1.7"
    ),
    region in ecr := Region.getRegion(Regions.US_WEST_2),
    repositoryName in ecr := (packageName in Docker).value,
    localDockerImage in ecr := (packageName in Docker).value + ":" + (version in Docker).value,
    login in ecr := ((login in ecr) dependsOn (createRepository in ecr)).value,
    push in ecr := ((push in ecr) dependsOn(publishLocal in Docker, login in ecr)).value
  )
  .dependsOn(core, cw)
  .enablePlugins(JavaAppPackaging, DockerPlugin, EcrPlugin)