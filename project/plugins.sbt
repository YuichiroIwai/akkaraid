addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")
addSbtPlugin("com.mintbeans" % "sbt-ecr" % "0.7.0")

libraryDependencies += "com.spotify" % "docker-client" % "3.5.13"