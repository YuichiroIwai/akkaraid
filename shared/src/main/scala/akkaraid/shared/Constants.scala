package akkaraid.shared

import com.typesafe.config.{Config, ConfigFactory}

object Constants {
  val appConfig: Config = ConfigFactory.load("app")
  val mapSize: Int = appConfig.getInt("map.size")
  val bossInitialHp: Long = appConfig.getInt("boss.initial-hp")
  val bossRecoverInterval: Int = appConfig.getInt("boss.recover-interval")
  val playerAllocateOnce: Int = appConfig.getInt("player.allocate-once")
  val playerAllocateInterval: Int = appConfig.getInt("player.allocate-interval")
  val fighterInitialAttack: Int = appConfig.getInt("fighter.initial-attack")
  val fighterInitialSpeed: Int = appConfig.getInt("fighter.initial-speed")
}
