package akkaraid.shared

import Constants._

case class Pos(x: Int, y: Int) {
  def isInPlayerField: Boolean = !isInBossField
  def isInBossField: Boolean = Pos.bossFields.contains(this)
  def distance(that: Pos): Double = Math.sqrt(Math.pow(that.x - x, 2) + Math.pow(that.y - y, 2))
  def +(that: Pos): Pos = Pos(x + that.x, y + that.y)
}

object Pos {
  val min: Int = 1
  val max: Int = mapSize
  val bossFields: Set[Pos] = Set(Pos(min, min), Pos(min, max), Pos(max, min), Pos(max, max))
  def random(currentMax: Int): Pos = Pos((Math.random() * currentMax + 1).toInt, (Math.random() * currentMax + 1).toInt)
  def randomFromPlayerField(areaId: Int): Pos = Area.randomPos(areaId) match {
    case p if p.isInPlayerField => p
    case _ => randomFromPlayerField(areaId)
  }
}

object Area {
  private val half = mapSize / 2
  def randomPos(areaId: Int): Pos = Pos.random(half) + toPos(areaId)
  def fromPos(pos: Pos): Int = pos.x / (half + 1) + ((pos.y / (half + 1)) << 1)
  def toPos(areaId: Int): Pos = Pos((areaId & 1) * half, ((areaId >> 1) & 1) * half)
}

