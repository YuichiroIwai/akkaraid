package akkaraid.shared

case class AppStats(playerStats: PlayerStats, bossStats: Map[Pos, BossStats], fighterStats: FighterStats) {
  def addPlayer(): AppStats = copy(playerStats = playerStats.increment)
  def putBoss(pos: Pos, hp: Long, level: Int = 1): AppStats = copy(bossStats = bossStats.updated(pos, BossStats(level, hp)))
  def modifyFighterStats(f: FighterStats => FighterStats): AppStats = copy(fighterStats = f(fighterStats))
  def simple: String = Seq(
    playerStats.toString,
    bossStats.values.map(boss => (boss.level, boss.hp)).mkString(","),
    fighterStats.toString
  ).mkString(" ")
}
object AppStats {
  def empty: AppStats = AppStats(PlayerStats(0), Map.empty, FighterStats.empty)
}
case class PlayerStats(count: Int) {
  def increment: PlayerStats = PlayerStats(count + 1)
}
case class BossStats(level: Int, hp: Long)
case class FighterStats(waiting: Int, going: Int, fighting: Int, returning: Int)
object FighterStats {
  def empty: FighterStats = FighterStats(0, 0, 0, 0)
}
